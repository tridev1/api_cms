<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    protected $table ='doctor';
    protected $fillable = ['name','email','phone','info','user_id','image','is_delete'];
}
