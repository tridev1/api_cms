<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class order_list extends Model
{
    protected $table ='order_list';

    protected $fillable = [
        'time_id',
        'day',
        'customer_id',
        'clinic_schedule_id',
        'message',
        'is_delete'
    ];

    public function time() {
        return $this->hasOne(time_calendar::class, "id", "time_id");
    }

    public function customer() {
        return $this->hasOne(customer::class, "id", "customer_id");
    }

    public function schedule() {
        return $this->hasOne(ClinicSchedule::class, "id", "clinic_schedule_id");
    }
}
