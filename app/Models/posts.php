<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected $table ='posts';
    protected $fillable = ['title','slug','feature_image','content','status','is_delete'];
}
