<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\DoctorService;
use App\Http\Services\UserService;
use App\Models\doctor;
use App\Models\User;

class DoctorController extends Controller
{
    public function __construct(
        DoctorService $doctorService,
        UserService $userService
      ){
        $this->doctorService = $doctorService; 
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $data = $this->doctorService->show($kw);
        return Response()->json(array('data' => $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data1 = new User;
        $data1->fill($request->all());
        $data1->role == 10;
        $checkUsers = $this->userService->store($data1);
        if ($checkUsers == true) {
            $newUser = $this->userService->findnew_id();
            $data2 = new doctor;
            $data2->fill($request->all());
            $data2->user_id = $newUser->id;
            $check = $this->doctorService->store($data2);
            return Response()->json($check);die;
        }else {
            return Response()->json($checkUsers);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataDoctor = $this->doctorService->find($id);
        $dataDoctor->fill($request->all());
        $dataUser = $this->userService->find($dataDoctor->user_id);
        $dataUser->fill($request->all());
        $checkUser = $this->userService->store($dataUser);
        $img = $request->feature_image;
        if ($checkUser == true){
            $checkCustomer = $this->doctorService->update($dataDoctor,$img);
            return Response()->json($checkCustomer);die;
        }
            return Response()->json($checkUser);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = $this->doctorService->find($id);
        $doctor->is_delete = 1;$img = null;
        $check = $this->doctorService->update($doctor,$img);
        if ($check == true) {
            $user = $this->userService->find($doctor->user_id);
            $user->is_delete = 1;
            $check2 =  $this->userService->store($user);
            return Response()->json($check2);die;
        }else{
            return Response()->json($check);
        }
    }
}
