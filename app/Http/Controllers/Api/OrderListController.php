<?php

namespace App\Http\Controllers\Api;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CustomerService;
use App\Http\Services\UserService;
use App\Http\Services\OrderService;
use App\Models\customer;
use App\Models\User;
use App\Models\order_list;

class OrderListController extends Controller
{
    use ResponseTrait;

    protected $orderModel;

    public function __construct(
        CustomerService $customerService,
        UserService $userService,
        order_list $orderModel
      ){
        $this->customerService = $customerService;
        $this->userService = $userService;
        $this->orderModel = $orderModel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->orderModel->newQuery()->with(["time", "customer", "schedule" => function($q) {
            $q->with("service");
        }])->get();
        return $this->setResponse($services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $customer = $request->customer_id;
        if ($customer == null or $customer == "") {// if dat lich vs benhnhan moi
            $data1 = new User;
            $data1->fill($request->all());
            $data1->role == 1;
            $checkUsers = $this->userService->store($data1);
            if ($checkUsers == true) {
                $newUser = $this->userService->findnew_id();
                $data2 = new customer;
                $data2->fill($request->all());
                $data2->user_id = $newUser->id;
                $checkCustomre = $this->customerService->store($data2);//done save new user + new customer
                if ($checkCustomre == true) {
                    $newCustomre = $this->customerService->findnew_id();
                    $data3 = new order_list;
                    $data3->fill($request->all());
                    $data3->customer_id = $newCustomre->id;
                    $checkOrder = $this->orderService->store($data3);
                    return Response()->json($checkOrder);// done all
                }else{
                    return Response()->json($checkCustomre);// false
                }
            }else {
                return Response()->json($checkUsers);// false
            }
        }else{ // datlich vs benh nhan da co thong tin tai phong kham
            $data = new order_list;
            $data->fill($request->all());
            $check= $this->orderService->store($data);
            return Response()->json($check);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
