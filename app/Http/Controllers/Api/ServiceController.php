<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ServicesService;
use App\Models\services;

class ServiceController extends Controller
{   
    public function __construct(
        ServicesService $servicesService
      ){
        $this->servicesService = $servicesService; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $services = $this->servicesService->show($kw);
        return Response()->json(array('data' => $services));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new services;
        $data->fill($request->all());
        $check = $this->servicesService->store($data);
        return Response()->json($check);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $img = $request->image;
        $data = $this->servicesService->find($id);
        $data->fill($request->all());
        $check = $this->servicesService->update($data,$img);
        return Response()->json($check);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->servicesService->find($id);
        $data->is_delete = 1;$img = null;
        $check = $this->servicesService->update($data,$img);
        return Response()->json($check);
    }
}
