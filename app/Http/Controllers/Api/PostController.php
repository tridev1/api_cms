<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\PostsService;
use App\Models\posts;

class PostController extends Controller
{
    public function __construct(
        PostsService $postsService
      ){
        $this->postsService = $postsService; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $posts = $this->postsService->show($kw);
        return Response()->json(array('data' => $posts));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new posts;
        $data->fill($request->all());
        $check = $this->postsService->store($data);
        return Response()->json($check);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $img = $request->feature_image;
        $data = $this->postsService->find($id);
        $data->fill($request->all());
        $check = $this->postsService->update($data,$img);
        return Response()->json($check);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->postsService->find($id);
        $data->is_delete = 1;$img = null;
        $check = $this->postsService->update($data,$img);
        return Response()->json($check);

    }
}
