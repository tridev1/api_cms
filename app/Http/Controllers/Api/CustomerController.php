<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\CustomerService;
use App\Http\Services\UserService;
use App\Models\customer;
use App\Models\User;

class CustomerController extends Controller
{
    public function __construct(
        CustomerService $customerService,
        UserService $userService
      ){
        $this->customerService = $customerService; 
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kw = $request->keyword;
        $customer = $this->customerService->show($kw);
        return Response()->json(array('data' => $customer));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data1 = new User;
        $data1->fill($request->all());
        $data1->role == 1;
        $checkUsers = $this->userService->store($data1);
        if ($checkUsers == true) {
            $newUser = $this->userService->findnew_id();
            $data2 = new customer;
            $data2->fill($request->all());
            $data2->user_id = $newUser->id;
            $check = $this->customerService->store($data2);
            return Response()->json($check);die;
        }else {
            return Response()->json($checkUsers);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $dataCustomer = $this->customerService->find($id);
        $dataCustomer->fill($request->all());
        $dataUser = $this->userService->find($dataCustomer->user_id);
        $dataUser->fill($request->all());
        $checkUser = $this->userService->store($dataUser);
        if ($checkUser == true){
            $checkCustomer = $this->customerService->store($dataCustomer);
            return Response()->json($checkCustomer);die;
        }else {
            return Response()->json($checkUser);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = $this->customerService->find($id);
        $customer->is_delete = 1;
        $check = $this->customerService->store($customer);
        if ($check == true) {
            $user = $this->userService->find($customer->user_id);
            $user->is_delete = 1;
            $check2 =  $this->userService->store($user);
            return Response()->json($check2);die;
        }else{
            return Response()->json($check);
        }
    }
}
