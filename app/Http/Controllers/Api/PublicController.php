<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Services\ServicesService;
use App\Models\ClinicSchedule;
use App\Models\customer;
use App\Models\order_list;
use App\Models\time_calendar;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    use ResponseTrait;

    protected $customerModel;

    protected $orderModel;

    protected $servicesService;

    public function __construct(
        customer $customerModel,
        order_list $orderModel,
        ServicesService $servicesService
    )
    {
        $this->customerModel = $customerModel;
        $this->orderModel = $orderModel;
        $this->servicesService = $servicesService;
    }

    public function onGetListClinicSchedules(Request $request) {
        $serviceID = $request->get("service_id", null);
        if (!is_null($serviceID)) {
            $results = app(ClinicSchedule::class)->newQuery()->with(["times" => function($q) {
                $q->where("is_booking", 0);
            }])->where(["service_id" => $serviceID])->get();
            return $this->setResponse($results);
        } else {
            return $this->setResponse([]);
        }
    }

    public function onGetListOrders(Request $request) {
        $services = $this->orderModel->newQuery()->with(["time", "customer", "schedule" => function($q) {
            $q->with("service");
        }])->get();
        return $this->setResponse($services);
    }

    public function onCreateCustomer(Request $request) {
        $dataInsert = $request->only(['name','email','phone','date','cmt']);
        $clinicScheduleID = $request->post("clinic_schedule_id");
        $timeID = $request->post("time_id");
        try {
            $customerModel = $this->customerModel->newQuery()->create($dataInsert);
            $ScheduleModel = app(ClinicSchedule::class)->newQuery()->findOrFail($clinicScheduleID);
            $timeModel = app(time_calendar::class)->newQuery()->findOrFail($timeID);
            $dataOrder = [
                "time_id" => $timeModel->id,
                "customer_id" => $customerModel->id,
                "clinic_schedule_id" => $ScheduleModel->id,
            ];
            $this->orderModel->newQuery()->create($dataOrder);
            $ScheduleModel->times()->updateExistingPivot($timeModel->id, ["is_booking" => 1], false);
            return $this->setResponse(null, "Cập nhật thông tin thành công", 0);
        } catch (\Exception $exception) {
            return $this->setResponse(null, $exception->getMessage());
        }
    }
}
