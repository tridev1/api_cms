<?php
namespace App\Http\Services;

use App\Models\time_calendar;
use Illuminate\Support\Facades\DB;

class TimeClassService{
    function __construct(time_calendar $time_calendar){
        $this->time_calendar = $time_calendar;
    }
    public function getAll(){
        return $this->time_calendar->all();
    }

    public function show($kw){
        if(!$kw || empty($kw)){
            return $this->time_calendar->orderBy('id','desc')->get();
		}else{
            $data =  $this->time_calendar->where('name', 'like', "%$kw%")
                            ->orderBy('id','desc')
                            ->get();
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function find($id){
        return $this->time_calendar->find($id);
    }
    public function store($data){
        return $data->save();
    }

    public function destroy($id){
        $data = $this->time_calendar->where('id',$id)->delete();
        if ($data == 1){
            return true;
        }else {
            return false;
        }
    }
}