<?php
namespace App\Http\Services;

use App\histories;
use Illuminate\Support\Facades\DB;

class HistoriesService{
    function __construct(histories $histories){
        $this->histories = $histories;
    }

    public function showList($kw,$showMore,$id){
        if(!$kw || empty($kw)){
            if($showMore > 0){
            return $this->histories->where([['customer_id', '=', $id ],
            ['is_delete', '=', '0'],])
                ->paginate($showMore);
            }else{
            return $this->histories->where([['customer_id', '=', $id ],
            ['is_delete', '=', '0'],])
                ->paginate(10);
            }
		}else{
            $data =  $this->histories->where([['name', 'like', "%$kw%"],
                                ['is_delete', '=', '0'],])
                            ->orderBy('id','desc')
                            ->paginate(8);
            $data->withPath("?keyword=$kw");
            return $data;
		}
    }

    public function getAll($id){
        return $this->histories->where([['customer_id', '=', $id ],
        ['is_delete', '=', '0'],])->get();
    }

    public function storage($data){
        return $data->save();
    }

    // public function delete($id){
    //     $data = $this->histories->find($id);
    //     $data->status = -1;
    //     $data->save();
    // }
}