<?php
namespace App\Http\Services;

use App\Models\order_list;
use Illuminate\Support\Facades\DB;

class OrderService{
    function __construct(order_list $order_list){
        $this->order_list = $order_list;
    }

    public function show($kw){
      
    }
    public function getAll(){
        return $this->order_list->where('is_delete','=',0)->get();
    }

    public function find($id){
		    return $this->order_list->find($id);
    }

    public function store($data){
      return $data->save();
    }
    
   
}
?>